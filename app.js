'use strict';

var express = require('express'), app = express();

app.set('views', 'views');
app.set('view engine', 'jade');

// Constants
const PORT = 8080;

app.get('/', function (req, res) {
  res.render('home', {});
});

app.listen(PORT);
module.exports.getApp = app;

console.log('Running on http://localhost:' + PORT);
